export const LocalStorage = {
    get(key) {
        const stored = localStorage.getItem(key)
        if (!stored) {
            return null
        }
        try {
            const decoded = atob(stored)
            return JSON.parse(decoded)
        } catch (e) {
            return null
        }
    },
    set(key, value) {
        try {
            const json = JSON.stringify(value)
            const encoded = btoa(json)
            localStorage.setItem(key, encoded)
        } catch (e) {
            console.log(e.message)
        }
    }
}
