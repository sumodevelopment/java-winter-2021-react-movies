import { Route, Redirect } from "react-router-dom";
import { LocalStorage } from "../util/storage";
import { LOGIN_STORAGE_KEY } from "../Login/LoginAPI";
import { AppRoutes } from "../consts/AppRoutes";

export const PrivateRoute = props => {

    const session = LocalStorage.get(LOGIN_STORAGE_KEY)

    if (session === null) {
        return <Redirect to={ AppRoutes.Login }  />
    }

    return <Route {...props} />
}

export default PrivateRoute
