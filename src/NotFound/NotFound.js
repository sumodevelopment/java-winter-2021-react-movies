import { Link } from "react-router-dom";
import { AppRoutes } from "../consts/AppRoutes";

function NotFound() {
    return (
        <main>
            <h4>You're lost! 🗺</h4>
            <Link to={ AppRoutes.Movies }>Go back home</Link>
        </main>
    )
}

export default NotFound
