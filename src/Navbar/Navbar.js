import { NavLink } from "react-router-dom";
import { AppRoutes } from "../consts/AppRoutes";
import styles from './Navbar.module.css'

function Navbar() {
    return (
        <nav className={ styles.Navbar }>
            <ul className={ styles.NavbarList }>
                <li><NavLink to={ AppRoutes.Movies }>Movies</NavLink></li>
                <li><NavLink to={ AppRoutes.Profile }>Profile</NavLink></li>
            </ul>
        </nav>
    )
}

export default Navbar
