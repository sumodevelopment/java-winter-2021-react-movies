import MovieListItem from "./MovieListItem";

function MovieList({movies}) {

    return (
        <>
            { movies.map(movie => <MovieListItem key={ movie.id } movie={ movie }/>) }
        </>
    )
}

export default MovieList
