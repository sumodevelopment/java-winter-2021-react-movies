import MovieListItemHeader from "./MovieListItemHeader";
import Card from "../Card/Card";

function MovieListItem({movie}) {
    return (
        <Card>
            <MovieListItemHeader movie={ movie }/>
            <p>{ movie.description }</p>
        </Card>
    )
}

export default MovieListItem
