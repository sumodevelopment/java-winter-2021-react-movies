function MovieListItemHeader({ movie }) {
    return (
        <header>
            <img src={ movie.cover } alt={ movie.title } />
            <h4>{ movie.title }</h4>
        </header>
    )
}
export default MovieListItemHeader
