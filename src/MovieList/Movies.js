import { useEffect, useState } from "react"
import MovieList from "./MovieList"
import Container from "../hoc/Container";

function Movies() {

    const [ movies, setMovies ] = useState([])
    const [ searchText, setSearchText ] = useState('')

    useEffect(() => {

        fetch('https://noroff-movie-catalogue.herokuapp.com/v1/movies')
            .then(r => r.json())
            .then(response => response.data)
            .then(movies => {
                setMovies(movies)
            })
            .catch(error => {
                console.error(error.message)
            })
        return () => {
        }
    }, []) // No deps -> useEffect only runs once.

    const onSearchChange = e => {
        setSearchText(e.target.value.trim())
    }

    return (
        <Container>
            <input onChange={ onSearchChange }
                   className="SearchInput"
                   type="text"
                   placeholder="Search for a movie title..."/>

            <MovieList movies={ movies.filter((movie) => {
                return movie.title.toLowerCase().includes(searchText.toLowerCase())
            }) }/>
        </Container>

    )
}

export default Movies


