export const LOGIN_STORAGE_KEY = '--rmv-ss'

export const LoginAPI = {
    login({username, password}) {
        return fetch('https://noroff-movie-catalogue.herokuapp.com/v1/users/login', {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            body: JSON.stringify({user: {username, password}})
        })
            .then(r => r.json())
            .then(({ success, data, error = '' }) => {
                if (success) {
                    return data
                }
                // Go to the catch
                throw Error(error)
            })
    }
}
