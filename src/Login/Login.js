import { useState } from "react";
import { LOGIN_STORAGE_KEY, LoginAPI } from "./LoginAPI";
import { LocalStorage } from "../util/storage";
import { Redirect, useHistory } from "react-router-dom";

function Login() {

    const history = useHistory()

    const [ state, setState ] = useState({
        username: '',
        password: ''
    })
    const [ error, setError ] = useState('')

    const onInputChange = e => {
        setState({
            ...state,
            [e.target.id]: e.target.value
        })
    }

    const onLoginClick = async () => {
        try {
            const userId = await LoginAPI.login( state )
            LocalStorage.set( LOGIN_STORAGE_KEY, {
                id: userId,
                username: state.username,
            });
            history.push('/movies')
        } catch (e) {
            setError(e.message)
        } finally {
            console.log('Completed!')
        }

    }

    return (
        <main>
            <h1>Login to your movies app</h1>
            <form>
                <fieldset>
                    <label htmlFor="username">Username</label>
                    <input onChange={ onInputChange } id="username" type="text" placeholder="What's your username?"/>
                </fieldset>

                <fieldset>
                    <label htmlFor="password">Password</label>
                    <input onChange={ onInputChange } id="password" type="password"
                           placeholder="What's your password?"/>
                </fieldset>

                <button type="button" className="d-flex flex-align-center" onClick={ onLoginClick }>
                    <span className="material-icons">lock</span>
                    Login
                </button>
            </form>

            { error &&
                <p className="d-flex flex-align-center">
                    <span className="material-icons">error</span>
                    {error}
                </p>
            }

        </main>
    )
}

export default Login
