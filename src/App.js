import './App.css';
import { BrowserRouter, Switch } from 'react-router-dom';
import PublicRoute from "./hoc/PublicRoute";
import PrivateRoute from "./hoc/PrivateRoute"
import { AppRoutes } from "./consts/AppRoutes";
import Login from "./Login/Login";
import Movies from "./MovieList/Movies";
import Profile from "./Profile/Profile";
import NotFound from "./NotFound/NotFound";
import Navbar from "./Navbar/Navbar";

function App() {
    return (
        <BrowserRouter>
            <div className="App">
                <Navbar />
                <Switch>
                    <PublicRoute path={ AppRoutes.Login } exact component={ Login }/>
                    <PrivateRoute path={ AppRoutes.Movies } component={ Movies }/>
                    <PrivateRoute path={ AppRoutes.Profile } component={ Profile }/>

                    <PublicRoute path="*" component={ NotFound } />
                </Switch>
            </div>
        </BrowserRouter>
    );

}

export default App;
