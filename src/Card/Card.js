import './Card.css'

function Card({ children }) {
    return (
        <div className="Card">
            <section className="CardContent">
                { children }
            </section>
        </div>
    )
}

export default Card
